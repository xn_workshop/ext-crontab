<?php

use Xn\Crontab\Http\Controllers\CrontabController;
use Xn\Crontab\Http\Controllers\CrontabLogController;

Route::resource('crontabs', CrontabController::class);
Route::resource('crontab-logs', CrontabLogController::class);
Route::post('crontabs/checkSchedule', CrontabController::class .'@checkSchedule');