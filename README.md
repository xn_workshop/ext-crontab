Crontab extension for laravel-admin
======

## 安装

```bash
composer require xn/crontab
php artisan migrate
```

## 配置

在`config/admin.php`檔案的`extensions`設定部分，加上屬於這個擴充的配置
```php

    'extensions' => [

        'crontab' => [
            'enable' => true,
        ]
    ]

```

在伺服器中配置crontab

```
crontab -e
* * * * * php /your_web_dir/artisan autotask:run >> /dev/null 2>&1
```

## 訪問

```
https://your domain/admin/crontabs #定時任務列表
https://your domain/admin/crontab-logs #定時任務日誌列表
```


## License

Licensed under [The MIT License (MIT)](LICENSE).
