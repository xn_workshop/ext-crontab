<?php

namespace Xn\Crontab\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Xn\Admin\Traits\DefaultDatetimeFormat;

class CrontabLog extends Model
{
    use DefaultDatetimeFormat;

    protected $table = 'crontab_logs';
    protected $fillable = ['type'];

    public function crontab(){
        return $this->belongsTo(Crontab::class, 'cid', 'id');
    }
}
