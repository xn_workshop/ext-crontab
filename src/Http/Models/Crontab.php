<?php

namespace Xn\Crontab\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Xn\Admin\Traits\DefaultDatetimeFormat;

class Crontab extends Model
{
    use DefaultDatetimeFormat;

    protected $table = 'crontab';

    public function crontabLog(){
        return $this->hasMany(CrontabLog::class, 'cid', 'id');
    }
}
