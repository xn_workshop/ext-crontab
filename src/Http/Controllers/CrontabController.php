<?php

namespace Xn\Crontab\Http\Controllers;

use Cron\CronExpression;
use Xn\Admin\Controllers\HasResourceActions;
use Xn\Admin\Form;
use Xn\Admin\Grid;
use Xn\Admin\Layout\Content;
use Xn\Crontab\Http\Models\Crontab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;

class CrontabController extends Controller
{
    use HasResourceActions;

    const CRONTAB_TYPE = [
        'sql'=>'執行sql',
        'shell'=>'執行shell',
        'url'=>'請求url'
    ];
    const CRONTAB_STATUS = [
        'normal'=>'正常',
        'disable'=>'禁用',
        'completed'=>'完成',
        'expired'=>'過期'
    ];

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        $content->breadcrumb(
            ['text' => '定時任務', 'url' => '/crontabs'],
            ['text' => '列表']
        );
        return $content
            ->header('列表')
            ->description('定時任務')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $content->breadcrumb(
            ['text' => '定時任務', 'url' => '/crontabs'],
            ['text' => '編輯']
        );
        return $content
            ->header('編輯')
            ->description('定時任務')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $content->breadcrumb(
            ['text' => '定時任務', 'url' => '/crontabs'],
            ['text' => '新增']
        );
        return $content
            ->header('新增')
            ->description('定時任務')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Crontab);
        $grid->column('id', 'Id')->sortable();
        $grid->column('type','類型')->using(self::CRONTAB_TYPE)->label('default');
        $grid->column('title', '任務標題');
        $grid->column('maximums', '最大次數');
        $grid->column('executes', '已執行次數')->sortable();
        $grid->column('execute_at', '下次預計時間');
        $grid->column('end_at', '最後執行時間')->sortable();
        $grid->column('status','狀態')->sortable()->editable('select', self::CRONTAB_STATUS);
        $grid->column('created_at','建立時間');

        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', '任務標題');
            $filter->equal('type', '類型')->select(self::CRONTAB_TYPE);
            $filter->equal('status', '狀態')->select(self::CRONTAB_STATUS);

        });

        return $grid;
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Crontab);

        $form->text('title', '任務標題')->rules('required',['required'=>'任務標題不能为空']);
        $form->select('type','任務類型')->options(self::CRONTAB_TYPE)->help("1. URL類型是完整的URL位址，如：<code>http://www.yourhost.com/</code> ；<br>2. 如果你的伺服器php.ini 未開啟<code>shell_exec() </code> 函數，則不能使用URL類型和Shell類型模式！")->rules('required|in:url,sql,shell',['required'=>'任務類型不能為空','in'=>'參數錯誤']);
        $form->textarea('contents', '内容')->rows(3)->rules('required',['required'=>'內容不為空']);
        $form->text('schedule', '執行周期')->default('* * * * *')->help("請使用<code>Cron</code>表達式")->rules(function ($form) {
            $value = $form->model()->schedule;
            if (empty($value)){
                return 'required';
            }
            if (!CronExpression::isValidExpression($value)){
//                return 'max:0';
            }
        },['required'=>'執行週期不能為空','max'=>'執行週期 Cron 表達式錯誤']);

        $form->html("<pre><code>*    *    *    *    *
-    -    -    -    -
|    |    |    |    +--- day of week (0 - 7) (Sunday=0 or 7)
|    |    |    +-------- month (1 - 12)
|    |    +------------- day of month (1 - 31)
|    +------------------ hour (0 - 23)
+----------------------- min (0 - 59)</code></pre>");
        $checkSchedule_url = url(env('ADMIN_ROUTE_PREFIX', 'admin').'/crontabs/checkSchedule');
        $js = <<<EOF
            <script type="text/javascript">
                 function checkSchedule() {
                    var schedule = $("#schedule").val();
                    $.post("{$checkSchedule_url}", {"schedule":schedule,_token:LA.token},
                    function(data){
                        if (data.status == false){
                            toastr.error(data.message);
                            return false;
                        }
                    }, "json");
                }

                $(function(){
                    checkSchedule();
                    $("#schedule,#begin_at").blur(function(){
                        checkSchedule();
                    });
                });
            </script>
EOF;
        $form->html($js);

        $form->number('maximums', '最大執行次數')->default(0)->help("0為不限次數")->rules('required|integer|min:0',[
            'required'=>'最大執行次數不能為空',
            'integer'=>'最大執行次數必須為正整數',
            'min'=>'最大執行次數不能為負數',
        ]);
        $form->number('executes', '已執行次數')->default(0)->help("如果任務執行次數達到上限，則會自動把狀態改為“完成”
        如果已“完成”的任務需要再次運行，請重置本參數或調整最大執行次數並把下面狀態值改成“正常”");
        $form->datetime('begin_at', '開始時間')->default(date('Y-m-d H:i:s'))->help("如果設定了開始時間，則從開始時間計算；如果沒有設定開始時間，則以當前時間計算。")->rules('required|date',['required'=>'開始時間不能為空','date'=>'時間格式不正確']);
        $form->datetime('end_at', '結束時間')->default(date('Y-m-d H:i:s'))->help("如果需要長期執行，請把結束時間設定得盡可能的久")->rules('required|date',['required'=>'結束時間不能為空','date'=>'時間格式不正確']);
        $form->number('weigh', '權重')->default(100)->help("多個任務同一時間執行時，依照權重從高到底執行")->rules('required|integer',['required'=>'權重不能為空','integer'=>'權重必須為正整數']);
        $form->select('status', '狀態')->default('normal')->options(self::CRONTAB_STATUS)->rules('required|in:disable,normal,completed,expired',['required'=>'狀態不能為空','in'=>'參數錯誤']);

        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });

        return $form;
    }

    /**
     * validate schedule.
     *
     * @return json
     */
    public function checkSchedule(Request $request){
        $schedule = $request->post('schedule','');
        if (empty($schedule)){
            return Response::json(['status'=>false,'message'=>'執行週期不能為空']);
        }
        if (!CronExpression::isValidExpression($schedule)){
            return Response::json(['status'=>false,'message'=>'執行周期 Cron 表達式錯誤']);
        }
        return Response::json(['status'=>true,'message'=>'']);
    }
}
