<?php

namespace Xn\Crontab\Http\Controllers;

use Xn\Crontab\Http\Models\CrontabLog;
use Illuminate\Routing\Controller;
use Xn\Admin\Controllers\HasResourceActions;
use Xn\Admin\Grid;
use Xn\Admin\Form;
use Xn\Admin\Layout\Content;
use Xn\Admin\Show;

class CrontabLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        $content->breadcrumb(
            ['text' => '定時任務日誌', 'url' => '/crontab-logs'],
            ['text' => '列表']
        );
        return $content
            ->header('列表')
            ->description('定時任務日誌')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $content->breadcrumb(
            ['text' => '定時任務日誌', 'url' => '/crontab-logs'],
            ['text' => '詳情']
        );
        return $content
            ->header('詳情')
            ->description('定時任務日誌')
            ->body($this->detail($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CrontabLog());
        $grid->disableCreateButton();

        $grid->column('id', 'Id')->sortable();
        $grid->column('type', '類型')->using(CrontabController::CRONTAB_TYPE)->label('default');
        $grid->column('title', '任務標題');
        $grid->column('created_at', '執行時間');
        $grid->column('status', '狀態')->sortable()->using(['0'=>'失敗','1'=>'成功'])->display(function ($status) {
            if($status == '失敗'){
                return '<span class="label label-danger">'.$status.'</span>';
            }else{
                return '<span class="label label-success">'.$status.'</span>';
            }
        });

        $grid->actions(function ($actions) {
            $actions->disableEdit();
        });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', '任務標題');
            $filter->equal('type', '類型')->select(CrontabController::CRONTAB_TYPE);
        });
        return $grid;
    }


    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CrontabLog::findOrFail($id));

        $show->field('cid', __('任務ID'));
        $show->field('type', __('類型'))->using(CrontabController::CRONTAB_TYPE)->label();
        $show->field('title', __('任務標題'));
        $show->field('created_at', __('執行時間'));
        $show->field('status', __('狀態'))->using([0 => '失敗',1 => '成功']);
        $show->field('remark', __('執行結果'));

        $show->panel()->tools(function ($tools) {
            $tools->disableEdit();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CrontabLog);
        return $form;
    }
}
