<?php

namespace Xn\Crontab;

use Xn\Admin\Extension;

class Crontab extends Extension
{
    public $name = 'crontab';

    public $migrations = __DIR__.'/../migrations/';

    public $menu = [
        'title' => '定時任務',
        'path'  => 'crontab',
        'icon'  => 'fa-gears',
    ];
}
